#Captain Obvious

##What is it?
Simulation of recommendations in Allegro. Cart should look little bit like Allegro's one but with items from our recommendation system based on graph database (WIP), price and based on what other people buy together with these items (in future). Application was made in 24h on Braincode 2019 - Hackathon organized by Allegro.

##How to run?
###BackEnd
`mvn spring-boot:run'
###FrontEnd
when in webapp folder `run ng serve -o
